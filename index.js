// ==UserScript==
// @name         show opponent estimated ranking
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  show opponent estimated ranking
// @author       Jérôme Crété
// @match        https://mon-espace-tennis.fft.fr/palmares/*
// @grant        none
// @require http://code.jquery.com/jquery-3.2.1.min.js
// ==/UserScript==


(function() {
    'use strict';

    $('.dynatable tr').each(function(index) {
        if(index === 0)
            return true;

        var id = $(this).children()[0].children[0].href.split('/');
        id = id[id.length - 1];
        console.log(id);

        $.post( "simulation-classement/" + id, {})
        .done(function( data ) {
            console.log( "Data Loaded: " + data );
        });

        return false;
    });
})();
